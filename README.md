# Bookstack



## Source
LinuxServer.io [https://docs.linuxserver.io/images/docker-bookstack/#docker-compose-recommended-click-here-for-more-info](https://docs.linuxserver.io/images/docker-bookstack/#docker-compose-recommended-click-here-for-more-info)

## Docker cli
````
docker run -d \
  --name=bookstack \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Etc/UTC \
  -e APP_URL=yourbaseurl \
  -e DB_HOST=yourdbhost \
  -e DB_PORT=yourdbport \
  -e DB_USER=yourdbuser \
  -e DB_PASS=yourdbpass \
  -e DB_DATABASE=bookstackapp \
  -e QUEUE_CONNECTION= `#optional` \
  -p 6875:80 \
  -v /path/to/bookstack/config:/config \
  --restart unless-stopped \
  lscr.io/linuxserver/bookstack:latest
  ````